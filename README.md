# Legacy ASP.NET MVC application with .NET Core

> The ASP.NET Web Api Controllers inherit form ApiController class which are now deprecated on ASPNETCore. 

To achieve this functionality we'll pull a [Compatibility Shim](https://www.nuget.org/packages/Microsoft.AspNetCore.Mvc.WebApiCompatShim/). 
 
+ Now create a new ASP.NET Core project (Empty) 
    - `dotnet new web` 
+ Next Create a `Controllers` folder with 2 Class files as shown.  
    ```
    Controllers/ 
    Controllers/USSDController.cs
    Controllers/USSDResponse.cs
    wwwroot/
    obj/
    Properties/
    bin
    Program.cs
    Startup.cs
    myproject.csproj
    ```
+ Next, edit the `Startup.cs` file as follows 
    ```c# 
        using System;
        using System.Collections.Generic;
        using System.Linq;
        using System.Threading.Tasks;
        using Microsoft.AspNetCore.Builder;
        using Microsoft.AspNetCore.Hosting;
        using Microsoft.AspNetCore.Http;
        using Microsoft.Extensions.DependencyInjection;
        using Microsoft.Extensions.Logging;

        namespace ussd_net_core
        {
            public class Startup
            {
                // This method gets called by the runtime. Use this method to add services to the container.
                // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
                public void ConfigureServices(IServiceCollection services)
                {
                    services.AddMvc().AddWebApiConventions();
                }

                // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
                public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
                {
                    app.UseMvc(routes => {
                        routes.MapWebApiRoute("DefaultApi", "api/{controller}/{id?}");
                    });
                    
                    app.Run(async (context) =>
                    {
                        await context.Response.WriteAsync("Hello World!");
                    });
                }
            }
        }

    ``` 

+ Next  add the compat shim dependency 
    - `dotnet add package Microsoft.AspNetCore.Mvc.WebApiCompatShim --version 2.1.1`    
     
+ Now edit your controllers to look like so,
    - USSDController.cs 
        ```c#
            using Microsoft.AspNetCore.Mvc;
            using System;
            using System.Net;
            using System.Net.Http;
            using System.Text;
            using System.Web.Http;
            namespace USSD.Controllers
            {
                [Route("services")]
                public class USSDServicesController : ApiController
                {
                    [Route("ussd")]
                    [HttpPost,ActionName("ussd")]

                    public HttpResponseMessage httpResponseMessage([FromBody] UssdResponse ussdResponse)
                    {
                        HttpResponseMessage responseMessage;
                        string response;

                        if (ussdResponse.text == null)
                        {
                            ussdResponse.text = "";
                        }

                        if (ussdResponse.text.Equals("",StringComparison.Ordinal))
                        {
                            response = "CON USSD Demo in Action\n";
                            response += "1. Do something\n";
                            response += "2. Do some other thing\n";
                            response += "3. Get my Number\n";
                        }
                        else if (ussdResponse.text.Equals("1",StringComparison.Ordinal))
                        {
                            response = "END I am doing something \n";
                        }else if (ussdResponse.text.Equals("2",StringComparison.Ordinal))
                        {
                            response = "END Some other thing has been done \n";
                        }else if (ussdResponse.text.Equals("3",StringComparison.Ordinal))
                        {
                            response = $"END Here is your phone number : {ussdResponse.phoneNumber} \n";
                        }
                        else
                        {
                            response = "END Invalid option \n";
                        }

                        responseMessage = Request.CreateResponse(HttpStatusCode.Created,response);

                        responseMessage.Content = new StringContent(response, Encoding.UTF8, "text/plain");

                        return responseMessage;
                    }
                }

            }
        ``` 
    - USSDResponse.cs 
        ```c#  
            namespace USSD.Controllers
            {
                public class UssdResponse
                {
                    public string text { get; set; }
                    public string phoneNumber { get; set; }
                    public string sessionId { get; set; }
                    public string serviceCode { get; set; }

                }
            }
        ``` 
+ Now build your project 
    ```bash 
    $ dotnet restore 
    $ dotnet build 
    ``` 
+ Next run your project 
    `dotnet run` 

+ To test `POST` to `localhost:5000/services/ussd` as shown ![Postman Sample](https://github.com/AfricasTalkingLtd/africastalking.Net/blob/master/ScreenShots/ussdapp/PostmanSample.PNG?raw=true)
